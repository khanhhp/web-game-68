import React, { useEffect, useState } from "react";
import { Button } from "../../ui/button";
import { NavLink, useNavigate } from "react-router-dom";
// import logo from "../../public/logo192.png";

const Header = () => {
    const [scroll, setScroll] = useState(false);
    useEffect(() => {
        window.addEventListener("scroll", () => {
            setScroll(window.scrollY > 50);
        });
    }, []);
    const [menu, setMenu] = useState(
        [
            { text: "Trang Chủ", link: "/" },
            { text: "Hướng Dẫn", link: "huong-dan" },
            { text: "kinh nghiệm", link: "kinh-nghiem" },
            { text: "Trò chơi", link: "tro-choi" },
            { text: "khuyến mãi", link: "khuyen-mai" },
            { text: "liên hệ", link: "lien-he" }
        ]
    );
    return (
        <div className={`max-w-full w-full fixed top-0 z-[9999] ${scroll ? "bg-black opacity-90" : ""}`}>
            <div className="header flex items-center justify-between text-white container mx-auto ">
                <div className="logo max-w-32">
                    <a href="#"><img src={process.env.PUBLIC_URL + '/logo.png'} alt="logo" /></a>
                </div>
                <nav className="menu-header">
                    <ul className="flex">
                        {
                            menu.map((item, index) => {
                                return (
                                    <li className="pr-10 uppercase" key={index}>
                                        <NavLink to={item.link} className={({ isActive, isPending }) =>
                                            isPending ? "pending" : isActive ? "custom-hd-active" : "hover:text-yellow transition"
                                        }>
                                            {item.text}
                                        </NavLink>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </nav>
                <div className=" text-xs">
                    <Button className="border-solid border-white rounded-xl " >
                        Tải Ngay
                    </Button>
                    <Button className="bg-yellow rounded-xl py-2 px-6 ml-9 text-black">
                        Chơi Ngay
                    </Button>
                </div>
            </div>
        </div>

    )
}
export default Header;