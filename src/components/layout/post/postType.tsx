
import React, { useEffect } from "react";
import Post from "../../../post/post";

interface IProps {
    postID: number,
    cat: string,
    title: string,
    slug: string,
    des: string,
    content: string,
    view: number,
    image: string,
}
interface ChildProps {
    post: IProps[];
}
const PostType: React.FC<ChildProps> = ({ post }) => {
    const filterPost = post.filter((val, index) => {
        return (val.slug === 'huong-dan');
    })
    return (
        <div>
            {
                filterPost.map((item, index) => {
                    return (
                        <div className="relative">
                            <a href="#">
                                <div className="avt">
                                    <img src={process.env.PUBLIC_URL + item.image} alt="image" />
                                    <div className="">
                                        <h3>15</h3>
                                        <p>Thg 3</p>
                                    </div>
                                    <h2>{item.title}</h2>
                                </div>
                            </a>
                        </div>
                    )
                })
            }
        </div>
    )
}
export default PostType;