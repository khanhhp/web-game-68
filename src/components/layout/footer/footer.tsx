import React, { useState } from "react";
import { Button } from "../../ui/button";
import "./style.css"
import { forEachTrailingCommentRange } from "typescript";
import InputCP from "../../ui/input";

function Footer() {
    const [footerMenu, setFooterMenu] = useState([
        { text: "Trang chủ", link: "#" },
        { text: "Hướng dẫn", link: "#" },
        { text: "Kinh nghiệm", link: "#" },
        { text: "Trò chơi", link: "#" },
        { text: "Khuyến mãi", link: "#" },
    ])
    return (
        <div className="bg-footer-texture bg-center bg-cover pt-14 border-solid border-yellow border-t-[12px] relative">
            <div className=" relative container mx-auto">
                <div className="border-b-white border-solid border-b-[1px] flex justify-between">
                    <div className=" logo-ft max-w-[305px]">
                        <div className="logo ">
                            <a href="#"><img className="mx-auto" src={process.env.PUBLIC_URL + '/logo.png'} alt="logo" /></a>
                        </div>
                        <p className="text-white text-opacity-70" >68 game bài bar
                            | Trang chủ chính thức của 68 game bài -
                            Cổng game đổi thưởng trực tuyến đẳng cấp nhất Việt Nam.
                        </p>
                    </div>
                    <div className=" menu flex justify-between w-[70%]">
                        <div className="flex w-full justify-around">
                            <ul>
                                {
                                    footerMenu.map((item, index) => {
                                        return (
                                            <li className={`menu-ft-${index}`} key={index}><a href={item.link}>{item.text}</a></li>
                                        )
                                    })
                                }
                            </ul>
                            <ul>
                                <li className="menu-ft-0"><a href="#">Cần giúp đỡ ?</a></li>
                                <li><a href="#">Liên hệ</a></li>
                            </ul>
                        </div>
                        <div className="max-w-[360px] w-full">
                            <h3 className="mb-8"><span className="text-top text-yellow">Nhận tin mới</span></h3>
                            <p className="text-white text-opacity-70 max-w-[340px]">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.
                            </p>
                            <form action="#">
                                <div className="flex justify-between mt-[18px]">
                                    <input type="text" placeholder="EMAIL" className="py-4 pl-4 text-[18px] max-h-14" />
                                    <Button className="w-24 h-14 bg-yellow text-white font-semibold">
                                        Gửi
                                    </Button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div className="relative container mx-auto text-white flex justify-between pt-[61px] pb-[46px]">
                <div className="">
                    <p><a href="#">Về chúng tôi</a></p>
                </div>
                <div className="pr-12">
                    <p>Copyright 2024 68BG All Right Resrved</p>
                </div>
            </div>
        </div>

    )
}
export default Footer;