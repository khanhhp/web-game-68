import React, { ReactNode } from 'react';

interface DivProps {
    children: ReactNode;
    className?: string;

}

const BackgroundPage: React.FC<DivProps> = ({ children, className, }) => {
    return (
        <div className="bg-black" >
            {children}
        </div>
    );
};

export default BackgroundPage;
