import React, { HTMLInputTypeAttribute } from "react";

type Props = React.InputHTMLAttributes<HTMLInputElement>
function InputCP({ className, ...props }: Props) {

    return (
        <input
            {...props}
            className="my-custom-style outline-none text-white "
        />
    )
}
export default InputCP;