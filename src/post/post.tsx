import { useState } from "react"
import PostType from "../components/layout/post/postType"
import Tabs from "../pages/home/conponents/tabs"

interface IProps {
    postID: number,
    cat: string,
    slug: string,
    title: string,
    des: string,
    content: string,
    view: number,
    image: string,
}

const Post: React.FC = () => {
    const [baiHD, setBaiHD] = useState<IProps[]>([
        {
            postID: 1,
            slug: 'huong-dan',
            cat: 'Hướng dẫn',
            title: 'Cách chơi Rồng Hổ được chia sẻ từ cao thủtrăm trận trăm thắng',
            des: '',
            content: '',
            view: 123,
            image: ''
        },
        {
            postID: 2,
            slug: 'huong-dan',
            cat: 'Hướng dẫn',
            title: '',
            des: '',
            content: '',
            view: 300,
            image: ''
        },
        {
            postID: 3,
            slug: 'huong-dan',
            cat: 'Hướng dẫn',
            title: '',
            des: '',
            content: '',
            view: 333,
            image: ''
        },
        {
            postID: 4,
            slug: 'huong-dan',
            cat: 'Hướng dẫn',
            title: '',
            des: '',
            content: '',
            view: 456,
            image: ''
        },
        {
            postID: 5,
            slug: 'huong-dan',
            cat: 'Hướng dẫn',
            title: '',
            des: '',
            content: '',
            view: 455,
            image: ''
        },
        {
            postID: 6,
            slug: 'huong-dan',
            cat: 'Hướng dẫn',
            title: '',
            des: '',
            content: '',
            view: 666,
            image: ''
        },
        {
            postID: 7,
            slug: 'kinh-nghiem',
            cat: 'Kinh nghiệm',
            title: '',
            des: '',
            content: '',
            view: 666,
            image: ''
        },
    ])
    return (
        <div>
            <PostType post={baiHD} />
        </div>
    )
}

export default Post;