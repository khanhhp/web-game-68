import React from 'react';
import { Button } from './components/ui/button';
import Header from './components/layout/header/header';
import Footer from './components/layout/footer/footer';
import HomePage from './pages/home/home';
import { Routes, Route, Link } from "react-router-dom";
import CatPage from './pages/cat/cat';

function App() {
  return (
    <div>
      <Header />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/huong-dan" element={<CatPage />} />
        <Route path="/kinh-nghiem" element={<CatPage />} />
      </Routes>
      <Footer />
    </div>

  );
}

export default App;
