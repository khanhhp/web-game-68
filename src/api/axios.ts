import axios from "axios";

const instance = axios.create({
    baseURL: 'https://api.techgrocery.vn/api/v1/address/',
    headers: {
      "Content-Type": "Application/json",
  },
  });
export default instance;