import React, { useEffect, useState } from "react";
// import instance from "../../api/axios";
import axios from "axios";

interface IDataT {
    _id: string,
    province: string,
    province_code: string
}
interface IHuyen {
    province_code: string;
    district: string;
}
const Diachi = () => {
    const [tinhID, setTinh] = useState("");
    const [huyen, setHuyen] = useState<IHuyen[] | null>([]);
    const [dataT, setData] = useState<IDataT[]>([])
    const handleChonTinh = (e: any) => {
        let checkNumber = Number(e.target.value);
        if (Number.isFinite(checkNumber)) {
            setTinh(e.target.value);
        } else {
            setTinh("");
        }
    }
    const handleChonHuyen = () => {

    }
    //lay tinh
    useEffect(() => {
        axios.get('https://api.techgrocery.vn/api/v1/address/provinces').then(response => {
            setData(response.data.data);
        }).catch(error => {
            console.error('Lỗi khi lấy dữ liệu tỉnh thành:', error);
        });
    }, []);
    //lay huyện 
    useEffect(() => {
        setHuyen(null)
        if (Number.isFinite(Number(tinhID))) {
            axios.get(`https://api.techgrocery.vn/api/v1/address/provinces?province_code=${tinhID}`).then(
                res => {
                    setHuyen(res.data.data.districts);
                }
            ).catch(error => {
                console.error("Lỗi khi lấy dữ liệu", error);
            })
        }
    }, [tinhID])

    console.log(">>>>>>>> test master")
    return (
        <div>
            <select value={tinhID} onChange={handleChonTinh} >
                <option value="Chọn tỉnh thành" selected>Chọn tỉnh thành</option>
                {
                    dataT.map((item, index) => {
                        return (
                            <option key={index} value={item._id}>{item.province}</option>
                        )
                    })
                }
            </select>
            |
            <select defaultValue="Chọn tỉnh huyện" onChange={handleChonHuyen} >
                <option value="Chọn tỉnh huyện" >Chọn tỉnh huyện</option>
                {huyen &&
                    huyen.map((item, index) => {
                        return (
                            <option key={index} value={item.province_code}>{item.district}</option>
                        )
                    })

                }
            </select>
        </div>
    )
}
export default Diachi;