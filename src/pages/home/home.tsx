import React from "react";
import Banner from "./conponents/banner";
import Header from "../../components/layout/header/header";
import Footer from "../../components/layout/footer/footer";
import "./style.css"
import IntroCasino from "./conponents/introCasino";
import BackgroundPage from "../../components/ui/background";
import KhuyenMai from "./conponents/khuyenMai";
import PostType from "../../components/layout/post/postType";
import Post from "../../post/post";
import TinNong from "./conponents/tinNong";
import Diachi from "../lien he/diachi";

function HomePage() {
    return (
        <BackgroundPage>
            <div>
                <Banner />
                <div className="">
                    <IntroCasino />
                </div>
                <div className="max-w-[1140px] mx-auto">
                    <KhuyenMai />
                    {/* <Post /> */}
                    <TinNong />
                    <Diachi />
                </div>
            </div>
        </BackgroundPage>
    )
}
export default HomePage;