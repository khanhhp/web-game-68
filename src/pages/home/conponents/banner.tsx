import React, { ButtonHTMLAttributes, HtmlHTMLAttributes, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Button } from "../../../components/ui/button";
function BannerHome() {
    const [banner, setBanner] = useState([
        {
            title: { top: "KHUYẾN MÃI CÁ THÁNG TƯ", mid: "TẶNG 199K ", bot: 'CHO LẦN NẠP ĐẦU TIÊN' },
            des: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
            , botton: { link: '#', text: 'Chơi Ngay' },
            img: "play-game.png"
        },
        {
            title: { top: "KHUYẾN MÃI CÁ THÁNG TƯ", mid: "TẶNG 199K ", bot: 'CHO LẦN NẠP ĐẦU TIÊN' },
            des: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
            , botton: { link: '#', text: 'Chơi Ngay' },
            img: "play-game.png"
        },
        {
            title: { top: "KHUYẾN MÃI CÁ THÁNG TƯ", mid: "TẶNG 199K ", bot: 'CHO LẦN NẠP ĐẦU TIÊN' },
            des: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
            , botton: { link: '#', text: 'Chơi Ngay' },
            img: "play-game.png"
        },
    ]);
    const SlickArrowLeft = ({ currentSlide, slideCount, ...props }: any) => (
        <button
            {...props}
            className={
                "slick-prev slick-arrow" +
                (currentSlide === 0 ? " slick-disabled" : "")
            }
            aria-hidden="true"
            aria-disabled={currentSlide === 0 ? true : false}
            type="button"
        >
            Trước
        </button>
    );
    const SlickArrowRight = ({ currentSlide, slideCount, ...props }: any) => (
        <button
            {...props}
            className={
                "slick-next slick-arrow " +
                (currentSlide === slideCount - 1 ? " slick-disabled" : "")
            }
            aria-hidden="true"
            aria-disabled={currentSlide === slideCount - 1 ? true : false}
            type="button"
        >
            Sau
        </button>
    )
    const settings = {
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: <SlickArrowLeft />,
        nextArrow: <SlickArrowRight />,

    };
    return (
        <div className="h-banner bg-banner-home home pt-[155px] pb-[46px] relative">
            <div className="container mx-auto relative z-10">
                <Slider {...settings}>
                    {banner.map((item, index) => {
                        return (
                            <div key={index}>
                                <div className="flex justify-between">
                                    <div className="mt-24 max-w-[638px] pl-12">
                                        <h3 className="custom-text-bn-home" >{item.title.top}</h3>
                                        <h2 className="font-extrabold leading-[91px] text-white text-[75px] my-4" >{item.title.mid}</h2>
                                        <h3 className="custom-text-bn-home" >{item.title.bot}</h3>
                                        <p className="text-[16px] font-normal leading-5 text-white mt-[103px] mb-24">
                                            {item.des}
                                        </p>
                                    </div>
                                    <div className="relative">
                                        <img src={process.env.PUBLIC_URL + '/' + item.img} alt="logo" />
                                        <Button className="absolute py-5 px-12 bg-white text-yellow bottom-12 left-[-47px] font-bold text-[24px]">
                                            {item.botton.text}
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </Slider>
            </div>
        </div>
    )
}
export default BannerHome;