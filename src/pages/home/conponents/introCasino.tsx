import React from "react";

function IntroCasino() {
    return (
        <div className="container bg-intro-casino bg-no-repeat bg-cover mx-auto">
            <div className="ml-auto max-w-[364px] text-right pt-[159px] pb-[114px]" >
                <h2 className="text-white text-[40px] font-extrabold">Casino trực tuyến</h2>
                <div className="my-[27px] text-content text-white opacity-50 "><span>May 10,2024</span></div>
                <p className="text-content text-white opacity-50 text-right"> Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod tempor incididunt ut
                </p>
                <div className="mt-[121px]">
                    <a href="#" className="flex float-end text-2xl text-white ">CHƠI NGAY
                        <img src={process.env.PUBLIC_URL + "/icon-play.png"} alt="icon" />
                    </a>
                </div>
            </div>
        </div>
    )
}
export default IntroCasino;