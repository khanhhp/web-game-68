import React, { useState } from "react";
import Slider from "react-slick";

function KhuyenMai() {
    const [postKM, setPostKM] = useState([
        { id: 1, title: 'Khuyến mãi nạp lần đầu', link: '#' },
        { id: 2, title: 'Khuyến mãi nạp 50%', link: '#' },
        { id: 3, title: 'NẠP TRÊN 100K TẶNG X2', link: '#' },
        { id: 4, title: 'Khuyến mãi nạp lần đầu', link: '#' },
        { id: 5, title: 'Khuyến mãi nạp lần đầu', link: '#' },
        { id: 6, title: 'Khuyến mãi nạp lần đầu', link: '#' }

    ])
    const settings = {
        infinite: true,
        autoplay: false,
        autoplaySpeed: 3000,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
    };

    return (
        <div className="pt-[100px] pb-[117px]">
            <h2 className="text-white text-[35px] leading-[42px] font-bold mb-10">Khuyến mãi</h2>
            <div className="km-ct">
                <Slider {...settings}>
                    {postKM.map((item, index) => {
                        return (
                            <div className="max-w-[369px] mx-auto" key={index}>
                                <a className=" text-white block py-10 text-2xl text-center border-solid border-[1px] border-yellow " href={item.link}>
                                    {item.title}
                                </a>
                            </div>
                        )
                    })}
                </Slider>
            </div>
        </div>
    )
}
export default KhuyenMai;