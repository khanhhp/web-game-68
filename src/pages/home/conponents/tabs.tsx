import { tab } from "@testing-library/user-event/dist/tab";
import React, { useState } from "react";
import { inflate } from "zlib";
import { Button } from "../../../components/ui/button";
interface IProps {
    slug: string;
}
const Tabs: React.FC<IProps> = ({ slug }) => {
    const [postHome, setPostHome] = useState([
        {
            cat: "Hướng dẫn",
            slug: "huong-dan",
            title: "Cách chơi Rồng Hổ được chia sẻ từ cao thủ trăm trận trăm thắng",
            img: "/images/img3.png"
        },
        {
            cat: "Hướng dẫn",
            slug: "huong-dan",
            title: "Mậu binh không khó ! Nắm giữ bí kíp...",
            img: "/images/img4.png"
        },
        {
            cat: "Hướng dẫn",
            slug: "huong-dan",
            title: "Cách chơi Rồng Hổ được chia sẻ ...",
            img: "/images/img1.png"
        },
        {
            cat: "Hướng dẫn",
            slug: "huong-dan",
            title: "Cách chơi Rồng Hổ được chia sẻ ...",
            img: "/images/img2.png"
        },
        {
            cat: "Kinh Nghiệm",
            slug: "kinh-nghiem",
            title: "Cách chơi Rồng Hổ được chia sẻ ...",
            img: "/images/img3.png"
        },
        {
            cat: "Kinh Nghiệm",
            slug: "kinh-nghiem",
            title: "Cách chơi Rồng Hổ được chia sẻ ...",
            img: "/images/img4.png"
        },
        {
            cat: "Kinh Nghiệm",
            slug: "kinh-nghiem",
            title: "Cách chơi Rồng Hổ được chia sẻ ...",
            img: "/images/img1.png"
        },
        {
            cat: "Kinh Nghiệm",
            slug: "kinh-nghiem",
            title: "Cách chơi Rồng Hổ được chia sẻ ...",
            img: "/images/img2.png"
        },
    ]);

    const contentTab = postHome.filter((item, index) => {
        return (item.slug === slug)
    })
    return (
        <div className="flex flex-wrap gap-4 ">
            {contentTab.map((item, index) => {
                return (
                    <div key={index} className="tn-avatar relative">
                        <div className={`tn-avatar-${index}`}>
                            <a href="#">
                                <img src={process.env.PUBLIC_URL + item.img} alt="" />
                            </a>
                        </div>
                        <div className="absolute text-top text-white left-3 bottom-4">
                            <Button className="text-[14px] leading-4 px-[10px] py-[7px] bg-white text-yellow">{item.cat}</Button>
                            <h3 className="text-top text-white mt-3 max-w-[80%]">{item.title}</h3>
                        </div>

                    </div>
                )
            })}
        </div>
    )
}
export default Tabs;