import React, { useState } from "react";

const RankUser = () => {
    const [user, setUser] = useState([
        { user: "abcxyz123", rank: 9976 },
        { user: "abcxyz123", rank: 9976 },
        { user: "abcxyz123", rank: 9976 },
        { user: "abcxyz123", rank: 9976 },
        { user: "abcxyz123", rank: 9976 },
        { user: "abcxyz123", rank: 9976 },
        { user: "abcxyz123", rank: 9976 },
        { user: "abcxyz123", rank: 9976 },
        { user: "abcxyz123", rank: 9976 },
        { user: "abcxyz123", rank: 9976 },
    ])
    return (
        <div>
            <table>
                <tr>
                    <th>No.</th>
                    <th>User</th>
                    <th>Điểm</th>
                </tr>
                {
                    user.map((item, index) => {
                        return (
                            <tr key={index}>
                                <td>{index}</td>
                                <td>{item.user}</td>
                                <td>{item.rank}</td>
                            </tr>
                        )
                    })
                }

            </table>
        </div>
    )
}

export default RankUser;