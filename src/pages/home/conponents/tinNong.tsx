import React, { useState } from "react";
import Tabs from "./tabs";

const TinNong = () => {
    const [tab, setTab] = useState<string>("huong-dan");
    const handleTogleTab = (slug: string) => {
        setTab(slug);
    }
    return (
        <div className="flex justify-between">
            <div className="block max-w-[752px] w-full pb-8">
                <div className="flex items-center border-b-white border-opacity-30 border-[1px] pb-8 mb-7">
                    <h2 className="text-3xl text-white w-fit mr-10">Tin mới nóng</h2>
                    <ul className="grid grid-cols-3 gap-x-9 text-[20px] text-white cursor-pointer">
                        <li onClick={() => handleTogleTab("huong-dan")}>Hướng dẫn</li>
                        <li onClick={() => handleTogleTab("kinh-nghiem")}>Kinh nghiệm</li>
                        <li onClick={() => handleTogleTab("tro-choi")}>Trò chơi</li>
                    </ul>
                </div>
                <div>
                    {
                        tab === 'huong-dan' ? <Tabs slug={tab} />
                            :
                            tab === 'kinh-nghiem' ? <Tabs slug={tab} />
                                : "tro choi"
                    }
                </div>
            </div>
            <div className="max-w-[370px]">
                <h2 className="text-3xl text-white border-b-white border-opacity-30 border-b-[1px] pb-8 mb-7">Xếp hạng người chơi</h2>
            </div>
        </div>
    )
}
export default TinNong;