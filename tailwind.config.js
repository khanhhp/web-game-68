/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    colors:{
      "black":"#000000",
      "yellow":'#E5AF11',
      "white" : "#FFFFFF",
      "transparent":'transparent'
    },
    fontSize: {
      '2xl': ['24px', {
        lineHeight: '29px',
        fontWeight:'600'
      }],
      "3xl" : ['35px', {
        lineHeight: '43px',
        fontWeight:'600'
      }], 
      // Or with a default line-height as well
      'top': ['18px', {
        letterSpacing: '-0.02em',
        lineHeight: '21px',
        fontWeight:'bold',
      }],
      'content':  ['16px', {
        lineHeight: '19px',
        fontWeight:'400',
      }],

    },
    container: {
      padding: {
        DEFAULT: '1rem',
        sm: '2rem',
        lg: '4rem',
        xl: '5rem',
        '2xl': '6rem',
      },
    },
    extend: {
      backgroundImage: {
        'footer-texture': "url('/public/bgr-footer.png')",
        'icon-search': "url('/public/icon-search.png')",
        'banner-home': "url('/public/banner-home.png')",
        'intro-casino':"url('/public/intro-casino.png')",
      },
    },
  },
  plugins: [],
  
}